package id.ac.ub.papb.recycler1;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.TextView;

public class DetailActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.detail_biodata);

        TextView tvNim = findViewById(R.id.tvNim2);
        TextView tvNama = findViewById(R.id.tvNama2);

        String nim = "Nim";
        String nama = "Nama";

        Bundle bundle = getIntent().getExtras();
        if(bundle != null){
            nim = bundle.getString("nim");
            nama = bundle.getString("nama");

        }
        tvNim.setText("NIM : " + nim);
        tvNama.setText("Nama : " + nama);


    }
}