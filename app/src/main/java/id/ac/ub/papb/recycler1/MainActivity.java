package id.ac.ub.papb.recycler1;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{

    RecyclerView rv1;
    public static String TAG = "RV1";
    private ArrayList<Mahasiswa> data;
    private MahasiswaAdapter.RecycleViewClickListener listener;
    EditText etNim, etNama;
    Button btnSimpan;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        rv1 = findViewById(R.id.rv1);
        data = getData();
        setOnClickListener();
        MahasiswaAdapter adapter = new MahasiswaAdapter(this, data,listener);
        rv1.setAdapter(adapter);
        rv1.setLayoutManager(new LinearLayoutManager(this));

        etNim = findViewById(R.id.etNim);
        etNama = findViewById(R.id.etNama);

        btnSimpan = findViewById(R.id.bt_simpan);
        btnSimpan.setOnClickListener(this);
    }

    private void setOnClickListener() {
        listener = (v, position) -> {
            Intent intent = new Intent(getApplicationContext(),DetailActivity.class);
            intent.putExtra("nim", data.get(position).nim);
            intent.putExtra("nama", data.get(position).nama);
            startActivity(intent);
        };
    }

    public ArrayList getData() {
        ArrayList<Mahasiswa> data = new ArrayList<>();
        List<String> nim = Arrays.asList(getResources().getStringArray(R.array.nim));
        List<String> nama = Arrays.asList(getResources().getStringArray(R.array.nama));
        for (int i = 0; i < nim.size(); i++) {
            Mahasiswa mhs = new Mahasiswa();
            mhs.nim = nim.get(i);
            mhs.nama = nama.get(i);
            Log.d(TAG,"getData "+mhs.nim);
            data.add(mhs);
        }
        return data;
    }

    @Override
    public void onClick(View view) {
        if(!etNim.getText().toString().equals("") && !etNama.getText().toString().equals("")) {
            Mahasiswa mhs = new Mahasiswa();
            mhs.nim = etNim.getText().toString();
            mhs.nama = etNama.getText().toString();
            data.add(mhs);
            Toast.makeText(this,"Data berhasil ditambahkan", Toast.LENGTH_SHORT).show();
            etNim.setText("");
            etNama.setText("");
            etNama.setFocusable(false);
        } else {
            if (etNim.getText().toString().equals("")) etNim.setError("NIM tidak boleh kosong");
            if (etNama.getText().toString().equals("")) etNama.setError("Nama tidak boleh kosong");
            Toast.makeText(this,"Data gagal ditambahkan, periksa kembali", Toast.LENGTH_SHORT).show();
        }

    }
}